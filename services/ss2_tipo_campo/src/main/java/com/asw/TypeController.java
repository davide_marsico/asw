package com.asw;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;




/**
 * 
 * @author Davide Marsico
 * @author Alessio Buscemi
 */

@Configuration
@PropertySource("application.yml")
@RestController
public class TypeController {



	@Value("${grounds}")
	private String grounds;

	@Value("${type}")
	private String type;




	@RequestMapping("/{campi}")
	public String getGrounds(@PathVariable String campi) {


		String[] campiArray = campi.split(",");
		String result = "";
		String campo;  
		String [] type_ground;


		for(int i = 0; i<campiArray.length-1; i++) {

			campo=extractGround(campiArray[i]);

			type_ground=getGroundType(campo);

			String tipoCampo = " " + campiArray[i] + " ( " + type_ground[0] + " su " + type_ground[1] + " ), ";

			result = result.concat(tipoCampo);

		}


		campo= extractGround(campiArray[campiArray.length-1]);

		type_ground=getGroundType(campo);

		String tipoCampo = " " + campiArray[campiArray.length-1] + " ( " + type_ground[0] + " su " + type_ground[1] + " ). ";

		result = result.concat(tipoCampo);		

		return result;

	}


	public String extractGround(String s){
		String campo = s.replace(" ", "");

		return campo.substring(0, 2);

	}

	private String[] type_ground_N1;
	private String[] type_ground_N2;
	private String[] type_ground_N3N4;
	private String[] type_ground_N5N6;


	@PostConstruct
	public void init(){
		String[] a={"idoor", "sintetico"};
		type_ground_N1=a;
		String[] a2={"outdoor", "cemento"};
		type_ground_N2=a2;
		String[] s={"outdoor ","terra"};
		type_ground_N3N4=s;
		String[] s2={"indoor","erba"};
		type_ground_N5N6=s2;

	}


	public String[] getGroundType(String campo){


		String[] tipiArray = type.split(",");
		String[] groundArray = grounds.split(",");
		String[] type_ground;

		switch(campo){

		case "N1": type_ground=type_ground_N1;
		break;

		case "N2": type_ground=type_ground_N2;
		break;

		case "N3": type_ground=type_ground_N3N4;
		break;

		case "N4": type_ground=type_ground_N3N4;
		break;

		case "N5": type_ground=type_ground_N5N6;
		break;

		case "N6": type_ground=type_ground_N5N6;
		break;

		default:
			int tipo = (int) (Math.round(Math.random()*(1)));
			int ground = (int) (Math.round(Math.random()*(groundArray.length-1)));
			String [] aux={tipiArray[tipo],groundArray[ground]};
			type_ground=aux;
			break;
		}

		return type_ground;
	}

}
