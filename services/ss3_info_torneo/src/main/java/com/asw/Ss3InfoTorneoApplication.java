package com.asw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ss3InfoTorneoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss3InfoTorneoApplication.class, args);
	}
}
