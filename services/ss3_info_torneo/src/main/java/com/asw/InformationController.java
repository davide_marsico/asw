package com.asw;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller gives informations about tournaments
 *
 * @author  Alessio Buscemi 
 */

@Configuration
@PropertySource("application.yml")
@RestController
public class InformationController {


	@Value("${grounds}")
	private String grounds;


	@RequestMapping("/{giorno}")
	public String getGrounds(@PathVariable String giorno) {

		String [] fields=getFields(giorno);

		String res = fields[0] + "," + fields[1] + "," + fields[2] + "," + fields[3];

		return res;
	}

	
	
	@RequestMapping("/{giorno}/{costo}")
	public String getTournamentFee(@PathVariable String giorno, @PathVariable String costo) {           

		String  res;

		if((giorno.equals("sabato")||giorno.equals("domenica"))&& Integer.parseInt(costo)<15)
			res="exception";

		else{
			String [] fields=getFields(giorno);
			int [] prices= getPrices(giorno,costo);

			res = fields[0] + " - prezzo: "+ prices[0] +" euro, "+  fields[1] + " - prezzo: "+ prices[1] +" euro, " +  fields[2] + " - prezzo: "+ prices[2] +" euro, " +  fields[3] +" - prezzo: "+ prices[3] +" euro.";
		}

		return res;
	}



	private String[] fields;
	private int [] prices;

	@PostConstruct
	public void init(){
		String[] aux={"N3","N4","N5","N6"};
		fields=aux;
		int [] aux2={15,15,15,15};
		prices=aux2;
	}


	public String[] getFields(String giorno){

		String [] res;

		if((giorno.equals("sabato")||giorno.equals("domenica")))
			res=fields;		
		else
			res=retrieveRandomFields(4);

		return res;
	}


	public int[] getPrices(String giorno, String costo){

		int [] res;

		if((giorno.equals("sabato")||giorno.equals("domenica")))
			res=prices;		
		else
			res=retrieveRandomPrices(costo);

		return res;
	}



	public String[] retrieveRandomFields(int n){

		ArrayList<Integer> list = new ArrayList<Integer>();
		String[] courtArray = grounds.split(",");


		for(int i=0; i<n; i++){
			int j= (int) (Math.round(Math.random()*(courtArray.length-1)));

			while(list.contains(j))
				j = (int) (Math.round(Math.random()*(courtArray.length-1)));

			list.add(j); 
		}

		String[] res=new String[n];
		int cont=0;

		for(int s: list){
			res[cont]=courtArray[s];
			cont++;		
		}

		return res;
	}

	
	public int[] retrieveRandomPrices(String costo){

		int max = Integer.parseInt(costo);

		int c1 = (int) (Math.round(Math.random()*(max)));
		int c2 = (int) (Math.round(Math.random()*(max)));
		int c3 = (int) (Math.round(Math.random()*(max)));
		int c4 = (int) (Math.round(Math.random()*(max)));

		int [] res={c1,c2,c3,c4};

		return res;
	}
}
