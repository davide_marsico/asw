package com.asw;

public interface TennisService {

	public String getGroundDay();
	
	public String getGroundHour(String day, String hour);
	
	public String getTypes(String grounds);
}
