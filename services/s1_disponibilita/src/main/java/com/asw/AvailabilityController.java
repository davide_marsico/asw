package com.asw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * This controller gives information about availables courts
 *
 * @author  Davide Marsico

 */


@Configuration
@PropertySource("application.yml")
@RestController
public class AvailabilityController {

	@Autowired
	private TennisServiceImpl tennisService;
	
	@RequestMapping("/{giorno}")
	public String getDisponibilitaGiorno(@PathVariable String giorno) {

		String grounds = tennisService.getGroundDay();

		String res = tennisService.getTypes(grounds);

		return "In questa settimana, per " + giorno + ", sono disponibili i seguenti campi: " + res;


	}

	@RequestMapping("/{giorno}/{ora}")
	public String getDisponibilitaGiorno(@PathVariable String giorno, @PathVariable String ora) {
		String grounds = tennisService.getGroundHour(giorno, ora);
		String res = tennisService.getTypes(grounds);

		return "In questa settimana, per " + giorno + " alle ore " + ora + " , sono disponibili i seguenti campi: " + res;
	}

}
