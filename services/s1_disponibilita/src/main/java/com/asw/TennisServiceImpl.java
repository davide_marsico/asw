package com.asw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asw.services.GroundClient;
import com.asw.services.TypeClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class TennisServiceImpl implements TennisService {
	
	@Autowired
	private TypeClient typeClient;
	@Autowired
	private GroundClient groundClient;

	@HystrixCommand(fallbackMethod="getFallbackGroundDay")
	public String getGroundDay() {
		return groundClient.getGroundDay();
	}

	@HystrixCommand(fallbackMethod="getFallbackGroundHour")
	public String getGroundHour(String day, String hour) {
		return groundClient.getGroundHour(day, hour);
	}

	@HystrixCommand(fallbackMethod="getFallbackTypes")
	public String getTypes(String grounds) {
		return typeClient.getTypes(grounds);
	}

	public String getFallbackGroundDay() {
		return "default";
	}
	
	public String getFallbackGroundHour(String day, String hour) {
		return day + " " + hour;
	}
	
	public String getFallbackTypes(String grounds) {
		return grounds;
	}
}
