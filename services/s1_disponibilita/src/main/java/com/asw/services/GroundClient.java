package com.asw.services;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("campi")
public interface GroundClient {
	
	@RequestMapping(value="/giorno", method=RequestMethod.GET)
	public String getGroundDay();
	
	@RequestMapping(value="/{giorno}/{ora}", method=RequestMethod.GET)
	public String getGroundHour(@PathVariable("giorno") String giorno, @PathVariable("ora") String ora);
	
}
