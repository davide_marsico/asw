package com.asw.services;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("tipo")
public interface TypeClient {
	
	@RequestMapping(value="/{grounds}", method=RequestMethod.GET)
	public String getTypes(@PathVariable("grounds") String grounds);
}
