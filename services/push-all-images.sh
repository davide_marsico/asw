#!/bin/bash

#DOCKER_REGISTRY=tenentedan9
DOCKER_REGISTRY=localhost:5000
#DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000

docker push ${DOCKER_REGISTRY}/zuul
docker push ${DOCKER_REGISTRY}/eureka
docker push ${DOCKER_REGISTRY}/disponibilita
docker push ${DOCKER_REGISTRY}/tornei
docker push ${DOCKER_REGISTRY}/campi
docker push ${DOCKER_REGISTRY}/tipo
docker push ${DOCKER_REGISTRY}/info
