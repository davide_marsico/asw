#!/bin/bash
#source "docker.env"

#DOCKER_REGISTRY=localhost:5000
#DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000
DOCKER_REGISTRY=tenentedan9

docker tag ${DOCKER_REGISTRY}/zuul ${DOCKER_REGISTRY}/zuul:$1
docker tag ${DOCKER_REGISTRY}/eureka ${DOCKER_REGISTRY}/eureka:$1
docker tag ${DOCKER_REGISTRY}/disponibilita ${DOCKER_REGISTRY}/disponibilita:$1
docker tag ${DOCKER_REGISTRY}/tornei ${DOCKER_REGISTRY}/tornei:$1
docker tag ${DOCKER_REGISTRY}/campi ${DOCKER_REGISTRY}/campi:$1
docker tag ${DOCKER_REGISTRY}/tipo ${DOCKER_REGISTRY}/tipo:$1
docker tag ${DOCKER_REGISTRY}/info ${DOCKER_REGISTRY}/info:$1
