source "docker.env"

#DOCKER_REGISTRY=localhost:5000
DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000
#DOCKER_REGISTRY=tenentedan9

docker build --rm -t ${DOCKER_REGISTRY}/zuul-9002 ./zuul
docker build --rm -t ${DOCKER_REGISTRY}/eureka-9002 ./eureka
docker build --rm -t ${DOCKER_REGISTRY}/disponibilita-9002 ./s1_disponibilita
docker build --rm -t ${DOCKER_REGISTRY}/tornei-9002 ./s2_tornei
docker build --rm -t ${DOCKER_REGISTRY}/campi-9002 ./ss1_campi_liberi
docker build --rm -t ${DOCKER_REGISTRY}/tipo-9002 ./ss2_tipo_campo
docker build --rm -t ${DOCKER_REGISTRY}/info-9002 ./ss3_info_torneo
