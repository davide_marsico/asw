#source "docker.env"

DOCKER_REGISTRY=localhost:5000
#DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000
#DOCKER_REGISTRY=tenentedan9

docker build --rm -t ${DOCKER_REGISTRY}/zuul ./zuul
docker build --rm -t ${DOCKER_REGISTRY}/eureka ./eureka
docker build --rm -t ${DOCKER_REGISTRY}/disponibilita ./s1_disponibilita
docker build --rm -t ${DOCKER_REGISTRY}/tornei ./s2_tornei
docker build --rm -t ${DOCKER_REGISTRY}/campi ./ss1_campi_liberi
docker build --rm -t ${DOCKER_REGISTRY}/tipo ./ss2_tipo_campo
docker build --rm -t ${DOCKER_REGISTRY}/info ./ss3_info_torneo
