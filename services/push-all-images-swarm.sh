#!/bin/bash

source "docker.env"

#DOCKER_REGISTRY=tenentedan9
#DOCKER_REGISTRY=localhost:5000
DOCKER_REGISTRY=swarm.inf.uniroma3.it:5000

docker push ${DOCKER_REGISTRY}/zuul-9002
docker push ${DOCKER_REGISTRY}/eureka-9002
docker push ${DOCKER_REGISTRY}/disponibilita-9002
docker push ${DOCKER_REGISTRY}/tornei-9002
docker push ${DOCKER_REGISTRY}/campi-9002
docker push ${DOCKER_REGISTRY}/tipo-9002
docker push ${DOCKER_REGISTRY}/info-9002
