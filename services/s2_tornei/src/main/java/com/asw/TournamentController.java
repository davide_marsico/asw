package com.asw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller gives information about daily tournaments and their fees
 *
 * @author  Alessio Buscemi

 */


@Configuration
@PropertySource("application.yml")
@RestController
public class TournamentController {	

	@Autowired
	private TennisServiceImpl tennisService;

	@RequestMapping("/{giorno}")
	public String getGiornoTorneo(@PathVariable String giorno) {

		String grounds = tennisService.getTournamentDay(giorno);

		String res = tennisService.getTypes(grounds);

		return "In questa settimana, " + giorno + ", si svolgono tornei nei seguenti campi: " + res;
	}

	@RequestMapping("/{giorno}/{costo}")
	public String getDisponibilitaGiorno(@PathVariable String giorno,@PathVariable String costo) {

		String res;

		String grounds = tennisService.getTournamentFee(giorno,costo);
		if(grounds.equals("exception"))
			res=giorno +" non ci sono tornei disponibili a meno di "+ costo+ " euro.";
		else {
			
			String type = tennisService.getTypes(grounds);
			res = "In questa settimana, " + giorno + ", il costo di iscrizione per i tornei,"
					+ " il cui prezzo e' inferiore a " + costo + " euro, e': " + type;
		}
		return res;
	}

}