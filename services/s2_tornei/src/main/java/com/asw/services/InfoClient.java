package com.asw.services;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("info")
public interface InfoClient {
	
	@RequestMapping(value="/{giorno}")
	public String getTournamentDay(@PathVariable("giorno") String giorno);
	
	@RequestMapping(value="/{giorno}/{costo}")
	public String getTournamentFee(@PathVariable("giorno") String giorno, @PathVariable("costo") String costo);
}
