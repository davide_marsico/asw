package com.asw;

public interface TennisService {

	public String getTournamentDay(String giorno);

	public String getTournamentFee( String giorno,  String costo);

	public String getTypes(String grounds);
}
