package com.asw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asw.services.InfoClient;
import com.asw.services.TypeClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class TennisServiceImpl implements TennisService {
	
	@Autowired
	private TypeClient typeClient;
	@Autowired
	private InfoClient infoClient;

	@HystrixCommand(fallbackMethod="getFallbackTournamentDay")
	public String getTournamentDay(String giorno) {
		return infoClient.getTournamentDay(giorno);
	}

	@HystrixCommand(fallbackMethod="getFallbackTournamentFee")
	public String getTournamentFee(String giorno, String costo) {
		return infoClient.getTournamentFee(giorno, costo);
	}

	@HystrixCommand(fallbackMethod="getFallbackTypes")
	public String getTypes(String grounds) {
		return typeClient.getTypes(grounds);
	}

	public String getFallbackTournamentDay(String day) {
		return day;
	}
	
	public String getFallbackTournamentFee(String giorno, String costo) {
		return giorno + " " + costo;
	}
	
	public String getFallbackTypes(String grounds) {
		return grounds;
	}
}
