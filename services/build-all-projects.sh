#!/bin/bash

mvn -f ./zuul install
mvn -f ./zuul package
mvn -f ./eureka install
mvn -f ./eureka package
mvn -f ./s1_disponibilita install
mvn -f ./s1_disponibilita package
mvn -f ./s2_tornei install
mvn -f ./s2_tornei package
mvn -f ./ss1_campi_liberi install
mvn -f ./ss1_campi_liberi package
mvn -f ./ss2_tipo_campo install 
mvn -f ./ss2_tipo_campo package
mvn -f ./ss3_info_torneo install 
mvn -f ./ss3_info_torneo package
