#!/bin/bash

mvn -f ./zuul clean
mvn -f ./eureka clean
mvn -f ./s1_disponibilita clean
mvn -f ./s2_tornei clean
mvn -f ./ss1_campi_liberi clean
mvn -f ./ss2_tipo_campo clean
mvn -f ./ss3_info_torneo clean
