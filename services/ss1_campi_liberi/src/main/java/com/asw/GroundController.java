package com.asw;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controllers gives some availables fields based on the specified day or specified day and hour
 *
 * @author  Davide Marsico 
 */

@Configuration
@PropertySource("application.yml")
@RestController
public class GroundController {


	@Value("${grounds}")
	private String grounds;

	private String[] fields;

	@PostConstruct
	public void init(){
		String[] aux={"N1","N2"};
		fields=aux;
	}


	@RequestMapping("/{giorno}")
	public String getGrounds(@PathVariable String giorno) {

		String [] courts=retrieveRandomFields(4);
		String res = courts[0] + "," +  courts[1] + "," + courts[2] + "," + courts[3];

		return res;
	}


	@RequestMapping("/{giorno}/{ora}")
	public String getGroundsOra(@PathVariable String giorno, @PathVariable String ora) {

		String [] courts=new String[2];

		if((giorno.equals("lunedi")||giorno.equals("martedi"))&&(ora.equals("12")||ora.equals("13")||ora.equals("14")||ora.equals("15")))
			courts=fields;
		else 
			courts=retrieveRandomFields(2);

		String res = courts[0] + "," +  courts[1];

		return res;
	}



	public String[] retrieveRandomFields(int n){

		ArrayList<Integer> list = new ArrayList<Integer>();
		String[] courtArray = grounds.split(",");


		for(int i=0; i<n; i++){
			int j= (int) (Math.round(Math.random()*(courtArray.length-1)));

			while(list.contains(j))
				j = (int) (Math.round(Math.random()*(courtArray.length-1)));

			list.add(j); 
		}

		String[] res=new String[n];
		int cont=0;

		for(int s: list){
			res[cont]=courtArray[s];
			cont++;		
		}

		return res;
	}
}
